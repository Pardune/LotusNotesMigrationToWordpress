'''
Created on 23 May 2018

@author: Birger Lueers
'''

from helperFunctions import xmlFunctions
from helperFunctions import wordpressFunctions
from helperFunctions import htmlFunctions
from helperFunctions import downloadFunctions


'''
Default attribute definitions
'''


'''
General Config
'''

# local working directory
workingDir = './workingDir/'


''' 
Lotus Notes Table

Webpage that we wan to download.
This webpage is considered the 'scrape'-object. Content from this source will be uploaded to a wordpress destination.
'''
dlSiteUrl = "your LN Table (i.e. webcontent) to be scraped"




'''
XML.

This file is exported by lotus Notes Designer.
It describes the layout of the Table. Holds some content: Date, attachments-meta, authors, ...
'''
lnXmlDesignerExport = "path to your LN-Designer exported XML"





'''
Wordpress

Variables in this field are denominated by the installation on
the wordpress server.
    wpDir     has to be set when installing a wordpress server
    wpSite    see multisite plugin for wordpress
    wpImages  for downloading Images in downloadFunctions.py
    wpPath    commandline
'''

# remote wordpress basedir. required for wp-CLI
wpDir = "/var/www/html/"

# site is a feature in wordpress. from optional multi-site addon
wpSite = "Name of Wordpress-site"

# images folder including site
wpImages = wpDir + wpSite + "media/images"

# command-line. In case wordpress' base path is not this upload's target path.
wpPath = "/var/www/html/"

# attachment folder
wpAttDir = wpDir + wpSite +  "attachments/"

# ssh login, to call wp-cli on the remote wordpress host. Password is asked seperately.
wpSsh = "user@host"

# IP or netname of wordpress server
wpIp = "nameOrip"



'''
Downloader

Configure the download functions.
'''

# Folders for Downloader to store content in locally,
dlAttDir = workingDir + 'Attachments/'
dlImgDir = workingDir +  'Images/'
dlBodyDir = workingDir +  'htmlBodyByUnid/'

# html body folder. LN-provided html bodies
dlBodyDirTxt = workingDir + 'htmlBodyByUnid/'



'''
main function
'''

def main():
    # Read existing meta-info from LN exported XML
    x = xmlFunctions.XmlFunctions(lnXmlDesignerExport)
    
    # currently passed to xmlFunctions
    h = htmlFunctions.HtmlFunctions(dlBodyDirTxt, wpImages, wpIp, wpSite)
    d = downloadFunctions.DownloadFunctions(dlSiteUrl)
    w = wordpressFunctions.WordpressFunctions(wpPath, wpSsh, wpIp)
    
    # switch-like loop to evaluate user input
    while True:
        #print (' (n) ')
        print ('Please choose what to do')
        print (' (1) XML-data to Py-objects')
        print (' (2) Download Bodies')
        print (' (3) Download Attachments')
        print (' (4) Download Images')
        print (' (5) Html-body to Py-objects, src replacement')
        print (' (6) Wordpress, create bare posts from XML')
        print (' (7) wordpress, attachments upload')
        print (' (9) Exit')
        opt = input()

        '''
        User Input evaluation
        '''
        
        # download
        if opt == '1':
            x.proc()
        elif opt == '2':
            d.downloadBodies(x.logbookEntries, dlBodyDir, dlSiteUrl)
        elif opt == '3':
            d.downloadAttachments(x.logbookEntries, dlAttDir)
        elif opt == '4':
            d.downloadImages(x.logbookEntries, dlImgDir, dlBodyDir)
        
        # body
        elif opt == '5':
            h.proc(x.logbookEntries, wpSite)
            
        # wordpress
        elif opt == '6':
            w.barePosts(x.logbookEntries)
        elif opt == '7':
            w.attachments(x.logbookEntries, wpAttDir)
        elif opt == '8':
            w.images(x.logbookEntries, wpImages)

        # end loop
        elif opt == '9':
            print('Exiting')
            exit()
            break
        else:
            # default case
            print('misunderstood. enter: 1, 2, ...\n')


if __name__ == '__main__':
    # run the actual main
    main()
