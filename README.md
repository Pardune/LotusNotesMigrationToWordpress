# Migrate LN

This program can be used to export Lotus Notes Sites to wordpress.
Sites to be exported used to be multi media Scientific Logbooks.

Requirements:
- Exported XML from Lotus Notes Designer. This file holds some content, and some meta-info of Sites.
- Lotus Notes webpage. This Table contains the existing webcontent.
- Wordpress. A remote host that runs wordpress and serves wp-CLI.

Optional:
- Media Server for offloading content from wp.

The program reads the XML, creates python objects, and lastly uploads to Wordpress.
'Python Objects' are: Attachments, html-bodies, pictures, authors, dates, ...

For user convenience, an interactive menu is implemented.
