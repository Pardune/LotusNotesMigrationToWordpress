'''
classdocs

This class holds a list of logbookEntry-objects.
It stores the xml data in spearated datafields:
logbookentries[]
    viewentries:
        unid
        post date
        subject
        author(s)
        attachment(s)
They are used to define 
'''


# imports
import re, json
from helperFunctions import logbookEntry

# etree imports 
try:
    from lxml import etree
    print("running with lxml.etree")
except ImportError:
    try:
        # Python 2.5
        import xml.etree.cElementTree as etree
        #print("running with cElementTree on Python 2.5+")
    except ImportError:
        try:
            # Python 2.5
            import xml.etree.ElementTree as etree
            #print("running with ElementTree on Python 2.5+")
        except ImportError:
            try:
                # normal cElementTree install
                import cElementTree as etree
                #print("running with cElementTree")
            except ImportError:
                try:
                    # normal ElementTree install
                    import elementtree.ElementTree as etree
                    #print("running with ElementTree")
                except ImportError:
                    print("Failed to import ElementTree from any known place")


'''
Iterate over         
'''

class XmlFunctions(object):
    # class definitions
    failedImports = []
    logbookEntries = []
    
    '''
    Iterate over 
    '''
    def runProcessing(self):
        # find all Entries
        i = 0
        viewEntries = self.root.findall('.//viewentry')
        for viewEntry in viewEntries:
            cur = None
            # failed imports detected here
            a = viewEntry.find('entrydata[@name="$0"]/text')
            if(not a is None):
                if a.text == "[Replication or Save Conflict]":
                    print("replication conflict found!")
                    self.failedImports.append(viewEntry.attrib['unid'])
                    continue
            
            # increase counter
            i += 1
            
            # unid
            unid = viewEntry.attrib['unid']
            
            # new logbookEntry-object to store all sanitized data
            cur = logbookEntry.LogbookEntry(unid)
            
            '''
            Creation time
            '''
            
            # Get the time when the entry was created for the post_date
            time_created = viewEntry.find('entrydata[@name="TimeCreated"]/datetime').text
            post_date_year = time_created[0:4]
            post_date_month = time_created[4:6]
            post_date_day = time_created[6:8]
            post_date_time = time_created[9:11] + ':' + time_created[11:13] + ':' + time_created[13:15]
            post_date =  post_date_year + '-' + post_date_month + '-' + post_date_day + ' ' +  post_date_time
            cur.dateTime = post_date
            
            # Get the Subject of the entry which is used for the post title
            # should be used for meta-info on the wordpress posts
            cur.subject = viewEntry.find('entrydata[@name="Subject"]/text').text


            '''
            Authors
            '''
            
            # Author can contain texlist (tag) or text (tag)
            entrydata_author = viewEntry.find('entrydata[@name="Autor"]/textlist')
            if not entrydata_author is None:
                # several authors
                authorsAll = entrydata_author.iter('text')
                for author in authorsAll:
                    cur.authors.append(author.text)
            else:
                # one author
                a = viewEntry.find('entrydata[@name="Autor"]/text')
                cur.authors.append(a.text)
                
            # now, sanitize author names, and create WordpressAuthorMeta in logbookentry
            for author in cur.authors:
                # First we are replacing german Umlaute and special characters
                author = re.sub('ä', 'ae', author)
                author = re.sub('ü', 'ue', author)
                author = re.sub('ö', 'oe', author)
                author = re.sub('ß', 'ss', author)
                
            # on to metaField
            data = '{"coAuthors": ' + ''.join(cur.authors) + '}'
            cur.authorsWpMeta = json.loads(data)
            
            print ("processed WordpressMeta authors are: " + cur.authorsWpMeta)
            
            '''
            Attachments
            '''
            
            # Attachments can contain texlist (tag) or text(tag), or empty text (tag)
            entrydata_attachments = viewEntry.find('entrydata[@name="AttachmentNames"]/textlist')
            if not entrydata_attachments is None:
                attachmentsAll = entrydata_attachments.iter('text')
                for attachment in attachmentsAll:
                    cur.attachments.append(attachment.text)
            else:
                # one attachment
                a = viewEntry.find('entrydata[@name="AttachmentNames"]/text')
                if not a.text is None:                                # No Attachment at all...
                    cur.attachments.append(a.text)
            
            
            '''
            This entry is done. Add it modified to the List, so we can just import it to wordpress when called.
            '''
            # Cur to list of logbookEntries
            self.logbookEntries.append(cur)
            
        print ('processed: ' , i)
        print ('failed imports: ' , self.failedImports)

        
    '''
    open xml and prepare tree. Then run.
    '''
    def proc(self):
        # create Tree
        try:
            self.tree = etree.parse(self.xml)
            self.root = self.tree.getroot()
        except Exception:
            print("Error on XML-file: Could not be loaded.")
        else:
            print ("successfully defined trunk and tree of XML. Continuing...")
            self.runProcessing()

        # wait for ENTER-key input before continuing
        input("\n press ENTER to continue")


    '''
    Constructor
        
    xml-filename as parameter
    '''
    
    def __init__(self, xml):
    # parse arguments
        self.xml = xml
        pass
        