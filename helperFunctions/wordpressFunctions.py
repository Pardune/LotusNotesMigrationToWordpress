from subprocess import check_output
import os
import urllib.parse

# alternatively, use the a local webserver to serve content.
# wp could downloaded from this webserver. 
# Discontinued in favor to the use of local ssh tunnel.
# content is side-loaded from 
#from helperFunctions import eigenHttpServer


'''
Wordpress Interface.

This class provides wordpress' functionality.
Additional commands can be added here.
'''


class WordpressFunctions(object):
    
    '''
    Create posts on wordpress.

    Customize import to wordpress here.
    '''
    def barePosts(self, logbookEntries):
        print("Importing Posts to Wordpress ... ")
        for entry in logbookEntries:
            wPostId = check_output(['wp', 'post', 'create', \
                                '--ssh=' + self.ssh, \
                                '--path=' + self.path, \
                                '--post_title=' + entry.subject, \
                                '--post_content=' + entry.body, \
                                '--post_date=' + entry.dateTime, \
                                '--meta_input=' + entry.authorsWp , \
                                '--porcelain'])
            wPostId = wPostId.decode("utf-8").strip()
            print("unid:" + entry.unid + " is wid:" + wPostId)
            entry.setWid(wPostId)
        pass
    
    # serve Attachments to wordpress host
    def attachments(self, logbookEntries, dlDir):
        print("Importing Media to Wordpress ... ")
        
        # copy files from local Download-folder to remote Media location.
        call = (str)("scp -rpv " + dlDir + " " + (self.ssh + ":" + self.path + "Attachments/"))
        print (call)
        os.system(call)
        for entry in logbookEntries:
            for attachment in entry.attachments:
                    wAttId = check_output(['wp', 'media', 'import', (dlDir + urllib.parse.quote(attachment)), '--ssh=' + self.ssh, '--path=' + self.path, '--porcelain'])
                    print ("attachment '", attachment, "' has ID: ", wAttId)
        pass
    
    
    '''
    Construtor
    '''
    def __init__(self, path, ssh, ip):
    # parse arguments
        self.path = path
        self.ssh = ssh
        self.ip = ip
        pass