from helperFunctions import htmlFunctions
import re

'''
This class alters bodies from downloaded LotusNotes html-files.
Location of pictures, links and so on can be changed here to match
the new wordpress-location.
'''

class HtmlFunctions(object):
    
    # return filename + extension 
    def processMatch(self,matchObject):
        matchString = matchObject.group(1)
        
        mediaInfo = re.search(r'This pattern is determined by Lotus Notes table. Find media that you want to store elsewhere.', matchString)
        returnStr = re.sub(mediaInfo.re, mediaInfo.group(1) + "." + mediaInfo.group(2), matchString)
        
        print ("returning string: " + returnStr)
        
        return returnStr
    
    # called by main: Option Nr. 2: ' (2) Html-body to Py-objects'
    def proc(self, logbookEntries, site):
        self.site = site
        for entry in logbookEntries:
            # open file by file, according to unid
            with open(self.textFilesDir + entry.unid + '.txt') as f:
                #unsanitizedBody = f.read()
                self.htmlSrc = f.read()
                
                '''
                Replace source of images, embedded in the html page.
                
                Directories for media can change in the new wordpress location. You can change the media location here.
                Regex with matching groups. 
                '''
                pattern = re.compile(r'<img src="(?P<urlOld>[\/\w\.]*?/Body/(?P<fileName>[\d\w\.]*?)\?[\S]*?\=(?P<fileExtension>\w{3}))"')
                matches = re.finditer(pattern, self.htmlSrc)
                for match in matches:
                    oldUrl = match.group(1)
                    name = match.group(2)
                    extension = match.group(3)
                    
                    newUrl = site + name + "." + extension
                    
                    self.htmlSrc = self.htmlSrc.replace(oldUrl, newUrl)
                    
                    #print (self.htmlSrc)
                #re.sub(match.group(1).re, match.group(2) + '.' + match.group(2), match.group(1))

            # finally, write body
            
            entry.setBody (self.htmlSrc)
            print (entry.body)    
                        
    
    #  called by xml functions
    def insertBody(self, unid):
        body = ""
        with open(self.textFilesDir + unid + '.txt') as f:
            #print (f.read())
            body = f.read()
        return body
    
    # Constructor
    def __init__(self, textFilesDir, imgDir, ip, site):
        # parse arguments
        self.textFilesDir = textFilesDir
        self.imgDir = imgDir
        self.ip = ip
        self.site = site
        self.htmlSrc = ""