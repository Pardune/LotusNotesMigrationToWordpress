'''
Meta Info of a logbook entry.
A List containing those Objects is stored in XMLfunctions.py .
Each object's data will be used to store via CLI, corresponding wordpress fields.
'''

class LogbookEntry(object):

    # wordpress Id by wordpressFunctions    
    def setWid(self, wid):
        self.wid = wid
        
    def setBody(self, body):
        self.body = body
        
    def __init__(self, unid):
        # parse arguments
        self.unid = unid
        print ("new object with unid: " + unid)
        
        # object fields
        self.post_date = ""
        self.subject = ""
        self.authors = []
        self.authorsWpMeta = None
        self.body = ''
        self.attachments = []
        self.embeddedPictures = []
        self.wid = 0
        