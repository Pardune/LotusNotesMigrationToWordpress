import urllib.request, urllib.error
import urllib.parse
import http.cookiejar
import getpass
import time
import os
import re

'''
Download Webpage-content with this program.
'''

class DownloadFunctions(object):
    # object fields
    
    # Login Page for Lotus Notes
    loginPageLN = "LotusNotes.Domain.de/Tabelle $ Login-Aktion"
    
    # checks, if username and pw are set
    def checkDefined(self):
        # Username and Password for Lotus Notes
        if self.defined == False:
            print("Lotus Notes user name:")
            username = input()
            password = getpass.getpass()
        
            # Get cookie and session for Lotus Notes Table
            cj = http.cookiejar.CookieJar()
            self.opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(cj))
            login_data = urllib.parse.urlencode({'Username' : username, 'Password' : password}).encode("utf-8")
            self.opener.open(self.loginPageLN, login_data)
            urllib.request.install_opener(self.opener)
            
            # set boolean
            self.defined = True
    
        
    '''
    HTML body
    
    Receive meta-data-list of logbookEntries.
    Download Bodies from existing online Lotus Notes Table.
    '''
    def downloadBodies(self, logBookEntries, bodyDir, lnUrl):
        self.checkDefined()
        for entry in logBookEntries:
            
            # create folders if they dont exist
            if not os.path.isdir(bodyDir):
                os.makedirs(bodyDir)
                
            filePath = bodyDir + entry.unid + ".txt"
            if not os.path.isfile(filePath):
                url = lnUrl + entry.unid + "/Body?OpenField"
                resp = self.opener.open(url)
                
                # write to disk
                with open(bodyDir + entry.unid + ".txt", 'wb') as f:
                    f.write(resp.read())
                    print ("body written: " + entry.unid)
            else:
                print("body existed: " , entry.unid)
            
    
    '''
    Images
    
    Receive meta-data-list of logbookEntries.
    Download Images from existing online Lotus Notes Table.
    '''
    def downloadImages(self, logbookEntries, imgDir, textFilesDir):
        self.checkDefined()
        for entry in logbookEntries:
            with open(textFilesDir + entry.unid + '.txt') as f:
                body = f.read()
                
                '''
                Download embedded images and save them to imgDir
                '''
                
                # iterator with pattern 
                iterator = re.compile('img src="([^"]*)"').finditer(body)
                for match in iterator:
                        # file name and format
                        print ("0:" + match.group(0), "1: " + match.group(1))
                        url = match.group(1)
                        
                        '''
                        The actual match pattern is determined by Lotus Notes.
                        It accesses Elements and Fields in the online body.
                        '''
                        m = re.match(r'match pattern', url)
                        
                        # concatenate url
                        dlUrl = self.lnUrl + entry.unid + '/Body/' + m.group(1) + 'Type. This is hidden here, too.' + m.group(2) 
                        
                        filePath = imgDir + m.group(1) + '.' + m.group(2)
                        if not os.path.isfile(filePath):
                            # connect
                            resp = self.opener.open (dlUrl)
        
                            # write
                            with open(filePath, 'wb') as f:
                                f.write(resp.read())
                                
                            print ("dl url: " , dlUrl)
                            time.sleep(0.5)
                        else:
                            print("image existed: " , m.group(1) + '.' + m.group(2))
         
         
    '''
    Attachments
    
    Receive meta-data-list of logbookEntries.
    Download Images from existing online Lotus Notes Table.
    '''
    def downloadAttachments(self, logbookEntries, dir):
        
        # folder needed
        if not os.path.isdir(dir):
            os.makedirs(dir)
            
        # available attachments
        count = 0
        
        # login defined?
        self.checkDefined()
        
        # store existing (hence not-overwritten) files
        duplicateFiles = []
        
        # store failed Http-gets
        failedHttps = []
        
        # iterate over logbookEntries and download Attachments, if not already present
        for entry in logbookEntries:
            unid = entry.unid
            for attachment in entry.attachments:
                # download if file not exists
                if not os.path.isfile(dir + attachment):
                    # download file
                    try:
                        resp = self.opener.open(self.lnUrl + unid + '/$File/' + urllib.parse.quote(attachment))
                        # write to disk
                        with open(dir + attachment, 'wb') as f:
                            f.write(resp.read())
                            print ("wrote: " + attachment)
                            count += 1
                            # delay to be nice to webserver
                            time.sleep(0.5)
                    except urllib.error.HTTPError:
                        failedHttps.append(unid,attachment)
                        print ("Url bad resolve: " + attachment + " from " + unid)
                else:
                    # file existed already. keep note.
                    print("Download existed: " + attachment)
                    duplicateFiles.append((unid,attachment))
                    count += 1
                    continue
        print ("\nalready existing files:", duplicateFiles)
        print ("failed Downloads:\n", failedHttps)
        print ("available Attachments: ", count)
    
    
    def __init__(self, lnUrl):
        # logIn info boolean
        self.defined = False
        self.lnUrl = lnUrl
